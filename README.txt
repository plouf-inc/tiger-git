
Number of suppressed lines, suppressed chunks, average chunks size:
.                             :  187  23   8
    lib                       :   16  11   1
        misc                  :   16  11   1
            symbol.hxx        :    6   4   1
            test-symbol.cc    :    1   1   1
            unique.hxx        :    8   5   1
            variant.hxx       :    1   1   1
    src                       :  171  12  14
        parse                 :  171  12  14
            parsetiger.yy     :  147   6  24
            scantiger.ll      :   18   3   6
            tiger-parser.cc   :    6   3   2
